
function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("header").classList.add('fixed');
        document.getElementById("online-recording").classList.add('fixed');
    } else {
        document.getElementById("header").classList.remove('fixed');
        document.getElementById("online-recording").classList.remove('fixed');
        
    }
}
window.onscroll = function () { scrollFunction() };
